<?php
/**
* The header for our theme
*
* This is the template that displays all of the <head> section and everything up until <div id="content">
		*
		* @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
		*
		* @package awesome_gardener
		*/
	?>
	<!doctype html>
	<html <?php language_attributes(); ?>>
		<head>
			<meta charset="<?php bloginfo( 'charset' ); ?>">
			<meta name="viewport" content="width=device-width, initial-scale=1">
			<link rel="profile" href="http://gmpg.org/xfn/11">
			<?php wp_head(); ?>
		</head>
		<body <?php body_class(); ?>>
			<div id="page" class="site">
				<!-- top header section -->
				<section id="top-header-area">
					<div class="container">
						<div class="row">
							<div class="header-contact-info col-lg-6 col-md-6 col-sm-7">
								<ul>
									<li><a href=""><i class="fa fa-phone"></i> (123) 456 789</a></li>
									<li><a href=""><i class="fa fa-envelope"></i> Info@gardencare.com</a></li>
								</ul>
							</div>
							<div class="header-search col-lg-6 col-md-6  col-sm-5">
								<li class="pull-right">
									<?php get_search_form(); ?>
								</li>
							</div>
						</div>
					</div>
				</section>				
				<!-- start header -->
				<header>
					<div class="container">
						<div class="logo pull-left">
							<div class="site-branding">
								<?php
								the_custom_logo();
								if ( is_front_page() && is_home() ) : ?>
								<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
								<?php else : ?>
								<p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></p>
								<?php
								endif;
								?>
								</div><!-- .site-branding -->
							</div>
							<div class="top-info pull-right">
								<div class="info-box">
									<div class="text-box">
										<p><span class="highlighted">Follow Us:</span></p>
										<ul class="social-icons">
											<li><a href="#"><i class="fa fa-facebook"></i></a></li>
											<li><a href="#"><i class="fa fa-twitter"></i></a></li>
											<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
											<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
											<li><a href="#"><i class="fa fa-vimeo"></i></a></li>
										</ul>
									</div>
								</div>
								<div class="info-box">
									<div class="icon-box">
										<i class="fa fa-map-marker"></i>
									</div>
									<div class="text-box">
										<p>Tyche collins, downtown <br>victoria, Australia</p>
									</div>
								</div>
								<div class="info-box">
									<div class="icon-box">
										<i class="fa fa-clock-o"></i>
									</div>
									<div class="text-box">
										<p>Mon - Sat 9.00 - 19.00 <br>Sunday Closed</p>
									</div>
								</div>
							</div>
						</div>
					</header>

					    <nav class="mainmenu-navigation stricky">
        <div class="container mainmenu-gradient-bg">
            <div class="navigation pull-left">
                <div class="nav-header">
                    <button><i class="fa fa-bars"></i></button>
                </div>                
                <?php
					wp_nav_menu( array(
						'theme_location' => 'menu-1',
						'menu_id'        => 'primary-menu',
					) );
				?>               
            </div>
            <div class="search-wrapper pull-right">
                <ul class="nav">
                    <li class="search-wrapper pull-right"><a class="search" href="contact-us.html">Make an appointment</a></li>
                </ul>
            </div>
        </div>
    </nav>



<!-- end header -->
<div id="content" class="site-content">